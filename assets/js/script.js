$(document).ready(function () {
	function disableNumpad(isDisabled, excludeInputs) {
		$('button[data-num]').not('button[' + excludeInputs + ']').each((i, v) => {
			$(v).prop('disabled', isDisabled);
		});
	}

	function removeAlert() {
		setTimeout(() => {
			$('.alert').remove();
		}, 5000)
	}

	$(document).on('insert', 'input[name="pin_code"]', () => {
		let pinCodeInput = $('form').find('input[name="pin_code"]');
		let pinCodeValue = $(pinCodeInput).val();

		if (pinCodeValue.length === 5) {
			disableNumpad(true, 'disabled');
			$('.alert').remove();

			let url = $(pinCodeInput).attr('data-type') === 'login' ? '/check-pin-code' : '/save-pin-code';

			$.ajax({
				url: url,
				dataType: 'JSON',
				type: 'POST',
				data: {
					pin_code: pinCodeValue,
				},
				success: (response) => {
					let type = $(pinCodeInput).attr('data-type');

					if (response.status === 200 && type === 'login') {
						window.location.replace(response.url);
					}

					if (response.status === 403 && type === 'login') {
						disableNumpad(false);

						$(pinCodeInput).val('');
						$(pinCodeInput).before('<div class="alert alert-danger alert-p" role="alert">' + response.message + '</div>');

						removeAlert();
					}

					if (response.status === 200 && type === 'config') {
						disableNumpad(false);

						$(pinCodeInput).val('');
						$(pinCodeInput).before('<div class="alert alert-success alert-p" role="alert">' + response.message + '</div>');

						removeAlert();
					}

					if (response.status === 403 && type === 'config') {
						disableNumpad(false);

						$(pinCodeInput).val('');
						$(pinCodeInput).before('<div class="alert alert-danger alert-p" role="alert">' + response.message + '</div>');

						removeAlert();
					}
				}
			});
		}
	});

	$(document).on('click', 'button[data-num]', function () {
		let pinCodeInput = $('form').find('input[name="pin_code"]');
		let pinCodeValue = $(pinCodeInput).val();
		let numpadNumber = $(this).attr('data-num');

		if (numpadNumber === 'x') {
			$(pinCodeInput).val(pinCodeValue.substring(-pinCodeValue.length, pinCodeValue.length - 1))
		} else {
			$(pinCodeInput).val($(pinCodeInput).val() + numpadNumber);
		}

		$(pinCodeInput).trigger('insert');
	});
});
