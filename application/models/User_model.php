<?php

class User_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	/**
	 * @param $id
	 * @return void
	 */
	public function changePinCode($id, $pinCode)
	{
		$user = $this->getUserByPinCode($pinCode);

		if (!empty($user)) {
			return false;
		}

		return $this->db
			->set([
				'pin_code' => md5($pinCode . 'codeigniter3')
			])
			->where([
				'user_id' => $id
			])
			->update('user');
	}

	/**
	 * @param $pinCode
	 * @return mixed
	 */
	public function getUserByPinCode($pinCode)
	{
		$query = $this->db->get_where('user', ['pin_code' => md5($pinCode . 'codeigniter3')]);

		return $query->result();
	}
}
