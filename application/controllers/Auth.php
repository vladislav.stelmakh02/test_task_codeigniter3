<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
	/**
	 * @return void
	 */
	public function login() : void
	{
		$user = $this->session->user_info;

		if (!isset($user)) {
			$data = [
				'title' => 'Codeigniter 3',
				'main_page_url' => base_url(),
			];

			$this->twig->render('auth/login', $data);
		}

		redirect(base_url('profile'));
	}

	/**
	 * @return void
	 */
	public function checkPinCode() : void
	{
		$this->load->model('user_model');

		$user = $this->user_model->getUserByPinCode($this->input->post('pin_code'));
		$user = array_shift($user);

		header('Content-Type: application/json');
		if (isset($user)) {
			$session_data = [
				'user_info' => [
					'id' => $user->user_id,
					'name' => $user->name
				]
			];

			$this->session->set_userdata($session_data);

			$body = [
				'status' => 200,
				'type' => 'login',
				'url' => base_url('profile'),
			];

			echo json_encode($body); die();
		}

		$body = [
			'status' => 403,
			'message' => 'Неправильный пин-код, попробуйте ещё раз!',
			'url' => '',
		];

		echo json_encode($body); die();
	}

	/**
	 * @return void
	 */
	public function savePinCode() : void
	{
		$this->load->model('user_model');

		$user = $this->session->user_info;

		$isSaved = false;
		if (isset($user)) {
			$isSaved = $this->user_model->changePinCode($user['id'], $this->input->post('pin_code'));
		}

		header('Content-Type: application/json');

		if ($isSaved) {
			$body = [
				'status' => 200,
				'message' => 'Пин-код был измен успешно!',
			];

			echo json_encode($body); die();
		}

		$body = [
			'status' => 403,
			'message' => 'Данный пин-код занят другим пользователем, попробуйте придумать другой.'
		];

		echo json_encode($body); die();
	}
}
