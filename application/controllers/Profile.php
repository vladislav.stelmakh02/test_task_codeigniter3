<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{
	public function index()
	{
		$user = $this->session->user_info;

		if (isset($user)) {
			$data = [
				'title' => 'Личный кабинет',
				'main_page_url' => base_url('profile'),
				'settings_url' => base_url('profile/settings'),
				'logout_url' => base_url('profile/logout?action=logout'),
				'user' => $user,
			];

			$this->twig->render('profile/profile', $data);
		}

		redirect(base_url('login'));
	}

	public function settings()
	{
		$user = $this->session->user_info;

		if (isset($user)) {
			$data = [
				'title' => 'Настройки',
				'main_page_url' => base_url('profile'),
				'settings_url' => base_url('profile/settings'),
				'logout_url' => base_url('profile/logout?action=logout'),
				'user' => $user
			];

			$this->twig->render('profile/settings', $data);
		}

		redirect(base_url('login'));
	}

	public function logout()
	{
		if ($this->input->get('action') === 'logout') {
			$this->session->unset_userdata('user_info');

			redirect(base_url('login'));
		}
	}
}
