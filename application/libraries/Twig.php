<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH . 'vendor/autoload.php';

use JetBrains\PhpStorm\NoReturn;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

class Twig
{
	/**
	 * @var Environment
	 */
	private Environment $twig;

	public function __construct()
	{
		$loader = new FilesystemLoader([
			FCPATH . 'application/views',
			FCPATH . 'assets'
		]);

		$this->twig = new Environment($loader, [
			'strict_variables' => false
		]);
	}

	/**
	 * @param $template
	 * @param array $data
	 * @return void
	 * @throws LoaderError
	 * @throws RuntimeError
	 * @throws SyntaxError
	 */
	public function render($template, array $data = []) : void
	{
		$template = $this->twig->load($template . '.twig');

		echo $template->render($data); die();
	}
}
